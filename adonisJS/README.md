# How to use it?

1. Install AdonisJS => [Documentation](https://www.adonisjs.com/docs/4.1/installation)
2. Import dependencies => ` > npm install `
3. Create webserver => ` > adonis serve --dev `


# Adonis fullstack application

This is the fullstack boilerplate for AdonisJs, it comes pre-configured with.

1. Bodyparser
2. Session
3. Authentication
4. Web security middleware
5. CORS
6. Edge template engine
7. Lucid ORM
8. Migrations and seeds

## Setup

Use the adonis command to install the blueprint

```bash
adonis new yardstick
```

or manually clone the repo and then run `npm install`.


### Migrations

Run the following command to run startup migrations.

```js
adonis migration:run
```
