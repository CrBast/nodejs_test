var app = require('express')(),
    express = require('express'),
    server = require('http').createServer(app),
    io = require('socket.io').listen(server);


app.get('/', function(req, res) {
   res.render('index_chat.ejs');
})


.use('/public', express.static('public'))


.get('/chat', function(req, res) {
    if(req.query.username != '' && req.query.username != null)
        res.render('chat.ejs', {username: req.query.username});
    else
        res.render('chat.ejs', {username: 'Spy '}); 
});

io.sockets.on('connection', function(socket){
    console.log('un utilisateur demande accès');

    socket.on('message', function(message)
    {
        socket.broadcast.emit('newMessage', {  user: socket.username, content: message});
    });

    socket.on('left', function()
    {
        socket.broadcast.emit('info', {  user: socket.username, type: "left"});
    });

    socket.on('newConnection', function (username) {
        console.log(username + ' Se connecte');
        socket.emit('state', {value: 'Online'})
        socket.broadcast.emit('info', { user: username , type: "connect"});
        socket.username = username;
    });
});

server.listen(8080);