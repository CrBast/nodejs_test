var express = require('express');
var favicon = require('serve-favicon'); // Charge le middleware de favicon
var cookieSession = require('cookie-session');



var app = express();

// Static routes
app.use('/public', express.static('public'));

app.use(cookieSession({
    name: 'session',
    keys: ['myVar']
}))



.get('/', function(req, res){
    res.setHeader('Content-Type', 'text/plain');
    res.send('Vous êtes à l\'acceuil'); 
})
.get('/sous-sol', function(req, res){
    res.setHeader('Content-Type', 'text/plain');
    res.send(res);
})

/*app.get('/etage/:etagenum/chambre', function(req, res) {

    res.setHeader('Content-Type', 'text/plain');

    res.end('Vous êtes à la chambre de l\'étage n°' + req.params.etagenum);

});*/

.get('/etage/:etagenum/chambre', function(req, res) {
    res.render('chambre.ejs', {etage: req.params.etagenum});
})

.get('/cookies/:action/:content*?', function(req, res) {
    switch (req.params.action) {
        case "show":
            if(req.session.myVar == null)
                res.send('Il faut la créer');
            else
                res.send(req.session.myVar)
            break;
    
        case "create":
            req.session.myVar = req.params.content;
            res.send('Its okay');
            break;
        case "remove":
            req.session = null; 
            res.send('null');       
        break;
    }
})

.get('/compter/:nombre', function(req, res) {
    var noms = ['Robert', 'Jacques', 'David'];
    res.render('page.ejs', {compteur: req.params.nombre, noms: noms});
})

.use(favicon(__dirname + '/public/lifebuoy.ico'))

.use(function(req, res, next){
    res.setHeader('Content-Type', 'text/plain');
    res.status(404).send('Page introuvable !');
});

app.listen(8080);