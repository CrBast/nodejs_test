var express = require('express');
var favicon = require('serve-favicon');
var session = require('express-session');

var app = express();

app.use(session({secret: 'todotopsecret'}))

.use('/public', express.static('public'))
.get('/', function(req, res) {
    if(req.session.list == null){
        req.session.list = [];
    }
    res.render('todolist.ejs', {list: req.session.list});
})

.post('/entry/:content', function(req, res){
    req.session.list.push(req.params.content);
    res.status(200).send('OK');
})

.delete('/entry/:id', function(req, res){
    if(req.session.list[req.params.id] == null)
        delete req.session.list[req.params.id];
})

.use(favicon(__dirname + '/public/to-do-list.ico'))

app.listen(8080);